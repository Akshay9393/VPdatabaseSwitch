﻿using System;
using System.IO;

namespace VPDataBaseSwitch
{
    internal static class Log
    {
        private static StreamWriter objFile;

        private static bool CreateLogFile()
        {
            try
            {
                if (!Directory.Exists(Directory.GetCurrentDirectory().ToString() + "\\Logs"))
                    Directory.CreateDirectory(Directory.GetCurrentDirectory().ToString() + "\\Logs");
                string str1 = Directory.GetCurrentDirectory().ToString();
                DateTime now = DateTime.Now;
                string str2 = now.ToString("yyyyMMdd");
                if (!File.Exists(str1 + "\\Logs\\VPDatabaseSwitchLog_" + str2 + ".Txt"))
                {
                    string str3 = Directory.GetCurrentDirectory().ToString();
                    now = DateTime.Now;
                    string str4 = now.ToString("yyyyMMdd");
                    File.Create(str3 + "\\Logs\\VPDatabaseSwitchLog_" + str4 + ".Txt").Close();
                }
                string str5 = Directory.GetCurrentDirectory().ToString();
                now = DateTime.Now;
                string str6 = now.ToString("yyyyMMdd");
                Log.objFile = new StreamWriter(str5 + "\\Logs\\VPDatabaseSwitchLog_" + str6 + ".Txt", true);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                Console.Read();
                return false;
            }
        }

        public static void MakeEntry(string strLog)
        {
            try
            {
                Log.CreateLogFile();
                if (Log.objFile == null)
                    return;
                Log.objFile.WriteLine(DateTime.Now.ToString() + " : " + strLog);
                Log.objFile.Close();
                Log.objFile.Dispose();
                Log.objFile = (StreamWriter)null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                Console.Read();
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
