﻿using Oracle.DataAccess.Client;
using System;
using System.Configuration;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Xml;

namespace VPDataBaseSwitch
{
    public class Program
    {

        private static string logFilePath = ConfigurationSettings.AppSettings[""];

        private static string resourceConfigFilePath = ConfigurationSettings.AppSettings["ResourceConfigFilePath"];

        private static string primaryResourceConfigFileName = ConfigurationSettings.AppSettings["PrimaryResourceConfigFileName"];

        private static string primaryIpAddress = ConfigurationSettings.AppSettings["PrimaryIpAddress"];

        private static string secondaryIpAddress = ConfigurationSettings.AppSettings["SecondaryIPAddress"];

        private static string primaryDBPort = ConfigurationSettings.AppSettings["PrimaryDBPort"];

        private static string secondaryDBPort = ConfigurationSettings.AppSettings["SecondaryDBPort"];

        private static string primaryDataSource = ConfigurationSettings.AppSettings["PrimaryDataSource"];

        private static string secondaryDataSource = ConfigurationSettings.AppSettings["SecondaryDataSource"];

        private static string primaryDBDataSource = ConfigurationSettings.AppSettings["PrimaryDBSource"];

        private static string secondaryDBDataSource = ConfigurationSettings.AppSettings["SecondaryDBSource"];

        private static string primaryDbUserID = ConfigurationSettings.AppSettings["PrimaryDBUserID"];

        private static string secondaryDbUserID = ConfigurationSettings.AppSettings["SecondaryDBUserID"];

        private static string primaryDatabasePWD = ConfigurationSettings.AppSettings["PrimaryDatabasePWD"];

        private static string secondaryDatabasePWD = ConfigurationSettings.AppSettings["SecondaryDatabasePWD"];

        private static string primaryHUSADBHOST = ConfigurationSettings.AppSettings["HUSADBPrimary"];

        private static string secondaryHUSADBHOST = ConfigurationSettings.AppSettings["HUSADBSecondary"];

        private static string resourceConfigHUSA = ConfigurationSettings.AppSettings["HUSAConfigFilePath"];


        public static void iisStop(string srvcName)
        {
            try
            {
                ServiceController serviceController = new ServiceController(srvcName);
                TimeSpan timeout = TimeSpan.FromSeconds(30.0);
                if (serviceController == null)
                    return;
                do
                {
                    serviceController.Refresh();
                }
                while (serviceController.Status == ServiceControllerStatus.ContinuePending || serviceController.Status == ServiceControllerStatus.PausePending || (serviceController.Status == ServiceControllerStatus.StartPending || serviceController.Status == ServiceControllerStatus.StopPending));
                if (serviceController.Status == ServiceControllerStatus.Running || serviceController.Status == ServiceControllerStatus.Paused)
                {
                    serviceController.Stop();
                    Console.WriteLine("IIS Stopped");
                    Log.MakeEntry("IIS Stopped");
                    serviceController.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                }
                serviceController.Close();
            }
            catch (Exception ex)
            {
                Log.MakeEntry(ex.Message);
            }
        }

        public static string getBetween(string strSource, string strStart, string strEnd)
        {
            int num1 = strSource.IndexOf(strStart);
            if (num1 != -1)
            {
                int startIndex = num1 + strStart.Length;
                int num2 = strSource.IndexOf(strEnd, startIndex);
                if (num2 > startIndex)
                    return strSource.Substring(startIndex, num2 - startIndex);
            }
            return string.Empty;
        }

        private static string GetString(byte[] bytes)
        {
            char[] chArray = new char[bytes.Length / 2];
            Buffer.BlockCopy((Array)bytes, 0, (Array)chArray, 0, bytes.Length);
            return new string(chArray);
        }

        private static string getConnectionString(string dbType)
        {

            string str1 = Program.Base64Decode(ConfigurationSettings.AppSettings["DBPassword"]);

            string str2;
            if (dbType == "Primary")
                str2 = "Data Source =(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = " + Program.primaryIpAddress + ")(PORT = " + Program.primaryDBPort + ")) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = " + Program.primaryDataSource + "))); User Id = " + Program.primaryDbUserID + ";Password =" + str1 + ";";
            else
                str2 = "Data Source =(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = " + Program.secondaryIpAddress + ")(PORT = " + Program.secondaryDBPort + ")) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = " + Program.secondaryDataSource + "))); User Id = " + Program.secondaryDbUserID + ";Password =" + str1 + ";";
            return str2;
        }

        public static string getHaResourceConnectionDetails(string dbType)
        {
            OracleConnection conn = new OracleConnection(Program.getConnectionString(dbType));
            string empty = string.Empty;
            try
            {
                conn.Open();
                OracleDataReader oracleDataReader = new OracleCommand("select Details from nvb_ha_resources where INTERNAL_NAME='DATABASE1'", conn).ExecuteReader();
                while (oracleDataReader.Read())
                    empty = oracleDataReader[0].ToString();
            }
            catch (Exception ex)
            {
                Log.MakeEntry(ex.Message);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
            return empty;
        }

        public static void iisStart(string servcName)
        {
            try
            {
                ServiceController serviceController = new ServiceController(servcName);
                if (serviceController == null)
                    return;
                do
                {
                    serviceController.Refresh();
                }
                while (serviceController.Status == ServiceControllerStatus.ContinuePending || serviceController.Status == ServiceControllerStatus.PausePending || (serviceController.Status == ServiceControllerStatus.StartPending || serviceController.Status == ServiceControllerStatus.StopPending));
                if (serviceController.Status == ServiceControllerStatus.Stopped)
                {
                    serviceController.Start();
                    Console.WriteLine("IIS Started.");
                    Log.MakeEntry("IIS Started.");
                    serviceController.WaitForStatus(ServiceControllerStatus.Running);
                }
                else if (ServiceControllerStatus.Paused == serviceController.Status)
                {
                    serviceController.Continue();
                    serviceController.WaitForStatus(ServiceControllerStatus.Running);
                }
                serviceController.Close();
            }
            catch (Exception ex)
            {
                Log.MakeEntry(ex.Message);
            }
        }

        public void checkWhichDatabasePointing(string database)
        {
            try
            {
                string connectionDetails = Program.getHaResourceConnectionDetails(database);
                if (connectionDetails != null && connectionDetails != "")
                    Program.getBetween(connectionDetails, "<ConnectionString>", "</ConnectionString>");
                if (!(database == "Primary"))
                {
                    if (!(database == "Secondary"))
                        return;
                    Program.iisStop("W3SVC");
                    Program.overWriteConfigFile(Program.resourceConfigFilePath, Program.secondaryDBDataSource, Program.secondaryDbUserID);
                    Console.WriteLine("Calling HUSA update config method to make Secondary Database active");
                    Program.overWriteHUSAConfigFile(Program.resourceConfigHUSA, Program.primaryHUSADBHOST, Program.secondaryHUSADBHOST);
                    Program.updateHaResourceTable("<ConnectionData name =\"DATABASE1\" type=\"OperationDB\"><DBEngineType>Oracle</DBEngineType><ConnectionString>" + Program.secondaryDBDataSource + "</ConnectionString><User>" + Program.secondaryDbUserID + "</User><Password>" + Program.secondaryDatabasePWD + "</Password></ConnectionData>", database);
                    Program.iisStart("W3SVC");
                }
                else
                {
                    Program.iisStop("W3SVC");
                    Program.overWriteConfigFile(Program.resourceConfigFilePath, Program.primaryDBDataSource, Program.primaryDbUserID);
                    Console.WriteLine("Calling HUSA update config method to make Primary Database active");
                    Program.overWriteHUSAConfigFile(Program.resourceConfigHUSA, Program.secondaryHUSADBHOST, Program.primaryHUSADBHOST);
                    Console.WriteLine("Done Calling HUSA update config method to make Primary Database active!");
                    Program.updateHaResourceTable("<ConnectionData name =\"DATABASE1\" type=\"OperationDB\"><DBEngineType>Oracle</DBEngineType><ConnectionString>" + Program.primaryDBDataSource + "</ConnectionString><User>" + Program.primaryDbUserID + "</User><Password>" + Program.primaryDatabasePWD + "</Password></ConnectionData>", database);
                    Program.iisStart("W3SVC");
                    Console.WriteLine("Done!");
                }
            }
            catch (Exception ex)
            {
                Log.MakeEntry(ex.Message);
            }
        }

        private static void Main(string[] args)
        {
            Console.WriteLine("*****************************************************************************");
            Console.WriteLine("**               Nuance VocalPassword Database Switch Tool.                **");
            Console.WriteLine("** Developed by - Nuance Communications, Inc., Professional Services India.**");
            Console.WriteLine("*****************************************************************************");
            Console.Write("\nSwitching to Database type ('Primary', 'Secondary') \nor Enter 'Password' to configure DB new password): ");
            string database = Console.ReadLine();
            Program program = new Program();
            if (database == "Password")
            {
                Console.Write("Enter Database New Password To Configure: ");
                string plainText = Console.ReadLine();
                if (plainText != null || plainText != "")
                {
                    Program.changePasswordValueInAppConfig(Program.Base64Encode(plainText));
                    Console.WriteLine("New Password is configured Successfully.");
                    Thread.Sleep(5000);
                    Environment.Exit(0);
                    Console.ReadKey();
                }
            }
            else if (database == "Primary" || database == "Secondary")
            {
                program.checkWhichDatabasePointing(database);
                Program.iisReset();
                Program.iisStop("NuanceVBDataReplicator");
                Program.iisStart("NuanceVBDataReplicator");
                Program.iisReset();
                Console.WriteLine("Done!");
            }
            else if (database != "Primary" || database != "Secondary" || database != "Password")
            {
                Console.WriteLine("Valid inputs are Primary, Secondary or Password");
                Console.Write("Exiting");
                for (int index = 0; index < 7; ++index)
                {
                    Console.Write(".");
                    Thread.Sleep(1000);
                }
                Environment.Exit(0);
                Console.ReadKey();
            }
            Console.ReadLine();
        }

        public static void iisReset()
        {
            Process process = new Process();
            try
            {
                process.StartInfo = new ProcessStartInfo("iisreset.exe");
                process.Start();
            }
            catch (Exception ex)
            {
                Log.MakeEntry(ex.Message);
            }
        }

        public static void overWriteConfigFile(
          string resourceConfigPath,
          string connectionString,
          string userID)
        {
            string filename = resourceConfigPath;
            XmlDocument xmlDocument = new XmlDocument();
            try
            {
                xmlDocument.Load(filename);
                foreach (XmlNode xmlNode in xmlDocument.GetElementsByTagName("ConnectionString"))
                    xmlNode.InnerText = connectionString;
                xmlDocument.Save(filename);
                Console.WriteLine("Config File is updated successfully.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Config Error Had Occured Please check Log File For Details");
                Log.MakeEntry(ex.Message);
            }
        }

        public static void updateHaResourceTable(string inputParameter, string database)
        {
            OracleConnection conn = new OracleConnection(Program.getConnectionString(database));
            try
            {
                conn.Open();
                new OracleCommand("update NVB_HA_RESOURCES set Details ='" + inputParameter + "' where INTERNAL_NAME = 'DATABASE1'", conn).ExecuteNonQuery();
                Console.WriteLine("Database is updated successfully.");
                //Console.WriteLine(inputParameter);
                Log.MakeEntry("Hello from"+inputParameter);
            }
            catch (OracleException ex)
            {
                Console.WriteLine("DataBase Error Had Occured Please check Log File For Details");
                Log.MakeEntry(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        public static string Base64Encode(string plainText)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(plainText));
        }

        public static void changePasswordValueInAppConfig(string encodedPassword)
        {
            try
            {
                System.Configuration.Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                configuration.AppSettings.Settings["DBPassword"].Value = encodedPassword;
                configuration.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
            }
            catch (Exception ex)
            {
                Log.MakeEntry(ex.Message);
            }
        }
        public static void overWriteHUSAConfigFile(
        string resourceConfigPath,
        string currentPrimaryHUSADB,
        string toBePrimaryHUSADB)
        {
            string filename = resourceConfigPath;
            XmlDocument xmlDocument = new XmlDocument();
            try
            {
                xmlDocument.Load(filename);
                XmlAttribute xmlAttribute = (XmlAttribute)xmlDocument.SelectSingleNode("//configuration/appSettings/add/@value");
                xmlAttribute.Value = xmlAttribute.Value.Replace(currentPrimaryHUSADB, toBePrimaryHUSADB);
                Console.WriteLine(xmlAttribute.Value);
                xmlDocument.Save(filename);
                Console.WriteLine("HUSAReport Config File is updated successfully at path:" + filename);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Config Error Had Occured Please check Log File For Details");
                Log.MakeEntry(ex.Message);
            }
        }
        public static string Base64Decode(string base64EncodedData)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(base64EncodedData));


        }
    }
}
